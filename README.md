# ASIC Design Checklist

## Design Description
- clock domains: frequencies, mayor blocks that receive them and synchronization between
- reset scheme
- startup sequence
- power domains
- flow: analog/digital on top, steps in which the chip is assembled (including equivalence check, LVS, DRC. pre-post processing ...)
- I/O interfaces (physical and logical)
- building blocs
    - functionality
    - specification
    - test setup and test procedure
- risk factors and single failure point (internal voltage regulator, PLL) and workarounds (direct powering, external clocks, reset)
- monitoring features
- verification environment and simulation enviroment
- design for test (how it will be tested)
- debugging capability (dedicated probe pads, bypass feature, back-up ports for configuration or readout)
- data management (repository organization)
- SEU/SET strategy (if required)
TID strategy (if required): 
    - choice of standard cell library and/or exclusion of particulat set of cells; 
    - constraints in the transistor size;
    - use of Enclosed Layouts in critical circuits 


## Functional Verification (plan)
- provide list of analog and digital corners (including process, extraction corner, uncertainty and derating factor/MC)
- provide list of all analog test (including specification and results of simulation ex. gain, noise), simulation detail level (extracted/schematic), corner results
- list all digital and mixed-signal scenarios (ex. particular configuration scenarios etc.), status and simulation level (RTL/SDF/spice)
    - standard operation
    - edge cases
    - randomization
    - physical testing procedures
- how modelling (if exist) has been proven to be correct, how is interface described (especially synchronous)

Special focus needs to be put on interface logic (between macros and logic).

# Implementation Verification
- is timing clean in all corners/modes
- are DRVs clean (capacitances, transition)
- are all the path constrained (check_timing)
- power and IR/EM analysis (digital and analog especially for power blocks)
- DRC/LVS (exceptions)
